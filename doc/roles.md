
# Install Debian

## ufw

- enable ufw, port 22 80 443 by default
- override with ufw_ports variable

## sshd

- configure sshd, no password

## debian_repositories

- configure sources.list
- override with debrepos variable

## etckeeper

- configure email address, distant git rep and ssh key

## exim4_sendgrid

- exim4 conf with sendgrid smtp, with fqdn
- define exim_domainname exim_mailname exim_password

## locales

- define locales
- override with mylocale variable

## apt_unattended

- unattended upgrades
- define alert_email


## bashrc

- define aliases, pwd
- email when connection

## nano

- install nano and nanorc for colors, lines, etc.


## admin_utils

- some utils packages: wget, curl, htop...

## purge_unwanted

- purge unwanted pkg


# Host Specific Settings

## timezone

- UTC
- override with mytimezone variable


# Hardening

## fail2ban

- install and configure jail.conf for sshd and nginx (error log?), inetutils-syslogd
- define alert_email

## rkhunter

- install and configure, email address
- define alert_email

## chkrootkit

- install and configure, email address
- define alert_email

## apparmor

- ...

## logwatch

- configure what to watch and email address to send reports
- define alert_email


# Install WPStack

## wpstack_www_structure

- create /srv/www/{{ fqdn }}/{cert,confs...,log,wp}

## wpstack_git_www

- ensure git is present 
- setup git for wp directory, gitignore

## wpstack_cert

- copy key and cert for fqdn
- selfsigned cert for default https server (no valid servername)

## wpstack_mariadb

- mariadb repositories
- copy wp.conf 
- install server and client

## wpstack_nginx

- nginx repositories
- install nginx
- copy basic config files 

## wpstack_php

- php72 rep
- install php pkg (list)
- configure php.ini in fpm and cli, and www.conf

## wpstack_redis

- install redis server

## wpstack_wp_cli

- download and install wpcli 

## wpstack_etckeeper_commit

- commit the config


# Install and Setup Wordpress

## wordpress_install

- dowload and deplay latest wp 

## wordpress_create_db

- create database and user with full priv on db

## wordpress_config

- configure wp config with template
- set nogenkey to not renew salt

## wordpress_plugins

- install plugins from list (but do not activate)

## wordpress_themes

- remove unwanted themes
- install themes

## wordpress_child_theme

- install divi child theme from template (fqdn, site name, theme name)

## wordpress_translations

- copy translations

## wordpress_attr

- chown and chmod on files and dir (+wpconfig, cert, conf)

## wordpress_git_commit

- commit the full install


