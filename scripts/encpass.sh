echo -n "$1" | ansible-vault encrypt_string --vault-id "$3" --stdin-name "$2"
